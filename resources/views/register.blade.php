<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>form</title>
</head>
<body>
  <form action="{{route('welcome')}}" method="get">
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <p>
        <label>First name:</label><br><br>
        <input type="text"><br><br>

        <label>Last name:</label><br><br>
        <input type="text"><br>
    </p>
    <p><label for="">Gender:</label><br><br>
        <input type="radio" id="radio1" name="radio1">
        <label for="">Male</label><br>
        <input type="radio" id="radio2" name="radio1">
        <label for="">Female</label><br>
        <input type="radio" id="radio3" name="radio1">  
        <label for="">Other</label><br>
    </p>
    <p><label>Nationality:</label><br><br>   
        <select name="" id="">
            <option value="">Indonesia</option>
            <option value="">Singapore</option>
            <option value="">Malaysia</option>
            <option value="">Australia</option>
        </select>
    </p>
    <p><label for="">Language Spoken:</label><br><br>
        <input type="checkbox">
        <label for="">Bahasa Indonesia</label><br>
        <input type="checkbox">
        <label for="">English</label><br>
        <input type="checkbox">
        <label for="">Other</label><br>
    </p>
    <p><label for="">Bio:</label><br><br>
        <textarea name="" id="" cols="30" rows="10"></textarea>
    </p>
    <button type="submit">Sign Up</button>
  </form>
</body>
</html>